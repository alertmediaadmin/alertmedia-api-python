----

When you first check out this repo, you should install pre-commit hooks:

```
pre-commit install
pre-commit install-hooks
```

Now your pre-commit linters are ready to go!

----
