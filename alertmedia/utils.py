import time
from operator import itemgetter
from itertools import groupby

def groupby_select_first(iterable, group_key, select_sort_key, group_reverse=False, select_reverse=False):
    it = list(iterable)
    it.sort(key=itemgetter(group_key), reverse=group_reverse)
    for _, group in groupby(it, itemgetter(group_key)):
        g = list(group)
        g.sort(key=itemgetter(select_sort_key), reverse=select_reverse)
        yield g[0]


class Utils(object):
    _iso_format = '%Y-%m-%dT%H:%M:%S.000Z'

    def _generate_recipient_tiers(self, users=None, groups=None, filters=None):
        if users is None:
            users = []
        if groups is None:
            groups = []
        # Recipient stuff
        recipient_tier = {'users': users, 'groups': groups}

        # Filter stuff
        if filters:
            for fltr in filters:
                pf = {'name': fltr['name']}
                if fltr.get('description'):
                    pf['description'] = fltr['description']
                pf['filter_parts'] = fltr['filter_parts']
                recipient_tier['filters'].append(pf)
        return recipient_tier

    def __init__(self, client):
        self.client = client

    def get_or_create_event(self, title=None, urgency='low'):
        if not title:
            title = "{} API Event".format(time.strftime("%m/%d/%Y"))

        events = self.client.event.list(title=title).data
        event = events and events[0]

        if not event:
            event_types = self.client.event.list_types(self.client.api_customer_id, default_only=True).data
            if len(event_types) != 1:
                raise Exception("Could not resolve default event_type")
            event_type_id = event_types[0].get('id')
            event = self.client.event.create(
                title=title,
                customer=self.client.api_customer_id,
                urgency=urgency,
                event_type=event_type_id,
            ).data

        return event

    def notify(self, message,
               event=None, formats="email",
               users=None, groups=None, filters=None, recipient_tiers=None, **kwargs):
        """
        Send a notification with a single call.
        
        Usage Example:
            >>> import alertmedia
            >>> client = alertmedia.Client()
            >>> client.utils.notify(message="Hey there!", formats="email", users=[136677])
        """
        event = self.get_or_create_event(event)
        recip_tiers = recipient_tiers or self._generate_recipient_tiers(users, groups, filters)
        if isinstance(formats, str):
            formats = formats.split(",")
        messages = []
        for fmt in formats:
            msg = kwargs.copy()
            msg.update({
                "message"     : message,
                "plain_text_message": message,
                "rich_text_message": f"<p>{message}</p>",
                "format"      : fmt,
            })
            messages.append(msg)
        return self.client.notification.create(
            message=message,
            messages=messages,
            event=event,
            target_formats=formats,
            users=users,
            groups=groups or [],
            recipient_tiers=recip_tiers
        )

    def get_plan(self, template, fallback_event_title="", partial_match=False, **kwargs):
        plans = self.client.notification.list_plans().data
        template = template.lower()
        matched = list(filter(lambda p: str(p.get('title')).lower() == template, plans))
        if partial_match and len(matched) != 1:
            matched = list(filter(lambda p: template in str(p.get('title')).lower(), plans))
        if partial_match and len(matched) != 1:
            matched = list(filter(lambda p: str(p.get('title')).lower() in template, plans))
        if len(matched) != 1:
            raise ValueError("Expected a single template but found %s matches." % len(matched))
        plan = matched[0]
        del plan['id']
        del plan['author']
        del plan['attachments']
        if 'messages' in plan and 'target_formats' in plan:
            plan['messages'] = list(groupby_select_first(
                (p for p in plan['messages'] if p['format'] in plan['target_formats']),
                group_key='format',
                select_sort_key='id',
                select_reverse=True
            ))
            for msg in plan['messages']:
                del msg['id']
                message = kwargs.get('message', None)
                if message:
                    msg['message'] = message
        plan.update(kwargs)
        if 'event' not in plan or not plan['event']:
            plan['event'] = self.get_or_create_event(fallback_event_title)
        return plan

    def notify_plan(self, template, fallback_event_title="", partial_match=False, **kwargs):
        return self.client.notification.create(**self.get_plan(template, fallback_event_title, partial_match, **kwargs))

    def list_filter_users(self, filter_name):
        users = []

        def extend_users(user_filters):
            resp = self.client.user.list(**user_filters)
            users.extend(resp.data)

        filters = [
            f for f in self.client.filter.list(name=filter_name).data
            if f and f["name"].lower() == filter_name.lower()
        ]
        if filters:
            if len(filters) > 1:
                raise ValueError("Found multiple filters: %s" % ", ".join(f["name"] for f in filters))
            filter_kwargs = {
                'customer': self.client.api_customer_id,
            }
            for part in filters[0]['filter_parts']:
                key = part['field_internal_name']
                if part['lookup_type'] != "iexact":
                    key += "__%s" % part['lookup_type']
                filter_kwargs[key] = part['value']
            extend_users(filter_kwargs)

        return users

