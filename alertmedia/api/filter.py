class CustomFilter(object):
    API_LIST_FILTERS = "/api/custom_filters"

    def __init__(self, client):
        self.client = client
        self.net = client.net

    def list(self, start=None, end=None, **kwargs):
        url = self.API_LIST_FILTERS
        return self.net.list(url, payload=None, start=start, end=end, query_params=kwargs)

