import os

from requests import ConnectionError
from requests.auth import HTTPBasicAuth
from alertmedia.api import User, Group, Notification, Event, CustomerAdmin, CustomFilter
from alertmedia.utils import Utils
from alertmedia.network import Network


class Client(object):
    def __init__(self, client_id=None, client_key=None, server="https://dashboard.alertmedia.com"):
        client_id = client_id or os.environ.get("AM_CLIENT_ID")
        client_key = client_key or os.environ.get("AM_CLIENT_SECRET_KEY")

        self.auth = HTTPBasicAuth(client_id, client_key)
        self.net = Network(self.auth, server)
        # get customer ID for this key
        login = self.net.get("/api/v2/login")
        if login.status_code != 200:
            raise ConnectionError("Unable to connect to %s. Reason: %s." % (self.net.base_url, login.reason))
        self.api_customer_id = login.get('customer.id')
        self.api_user_id = login.get('user.id')
        self.api_admin_id = login.get('admin.id')

    @property
    def user(self):
        return User(client=self)

    @property
    def admin(self):
        return CustomerAdmin(client=self)

    @property
    def group(self):
        return Group(client=self)

    @property
    def notification(self):
        return Notification(client=self)

    @property
    def event(self):
        return Event(client=self)

    @property
    def utils(self):
        return Utils(client=self)

    @property
    def filter(self):
        return CustomFilter(client=self)
