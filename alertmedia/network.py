import time
from collections import namedtuple
from requests import Session


try:
    from urllib.parse import urlparse, urlencode, urljoin, parse_qsl, urlunparse
except ImportError:
    from urlparse import urlparse, urljoin, parse_qsl, urlunparse
    from urllib import urlencode


class AlertMediaSession(Session):
    def __init__(self, auth):
        super(AlertMediaSession, self).__init__()
        self.auth = auth

    def request(self, *args, **kwargs):
        # Retry 5 times if throttled
        for r in range(5):
            response = super(AlertMediaSession, self).request(*args, **kwargs)
            if response.status_code != 429:
                return response
            time.sleep(5)


class Network(object):
    def __init__(self, auth, url):
        parsed = urlparse(url)
        self.base_url = "%s://%s" % (parsed.scheme or "https", parsed.hostname or parsed.path, )
        self.session = AlertMediaSession(auth)

    @staticmethod
    def append_query_params(url, query_params):
        url_parts = list(urlparse(url))
        query = dict(parse_qsl(url_parts[4]))
        query.update(query_params)
        url_parts[4] = urlencode(query)
        return urlunparse(url_parts)

    def get_url(self, end_point, query_params=None):
        url = urljoin(self.base_url, end_point)
        if query_params:
            url = self.append_query_params(url, query_params)
        return url

    @staticmethod
    def build_header(start=None, end=None, content_type="application/json"):
        if content_type is None:
            content_type = 'application/json'
        headers = {"Content-Type": content_type}
        if start is not None and end is not None:
            headers.update(**{"item-range": "items={}-{}".format(start, end),
                              "alertmedia-client-version": "1.0.8"})
        return headers

    def list(self, end_point, payload=None, start=None, end=None, query_params=None):
        r = self.session.get(self.get_url(end_point, query_params), headers=self.build_header(start, end), params=payload)
        return PagedResult(r, callback=self.list, callback_kwargs=dict(
            end_point=end_point, payload=payload, start=start, end=end, query_params=query_params))

    def get(self, end_point, payload=None, start=None, end=None, query_params=None):
        r = self.session.get(self.get_url(end_point, query_params=query_params), headers=self.build_header(start, end), params=payload)
        return Result(r)

    def post(self, end_point, payload=None, query_params=None, data=None, content_type=None):
        r = self.session.post(self.get_url(end_point, query_params), headers=self.build_header(content_type=content_type), json=payload, data=data)
        return Result(r)

    def put(self, end_point, payload=None, query_params=None, data=None):
        r = self.session.put(self.get_url(end_point, query_params), headers=self.build_header(), json=payload, data=data)
        return Result(r)

    def patch(self, end_point, payload=None, query_params=None, data=None):
        r = self.session.patch(self.get_url(end_point, query_params), headers=self.build_header(), json=payload, data=data)
        return Result(r)

    def delete(self, end_point, payload=None, query_params=None, data=None):
        r = self.session.delete(self.get_url(end_point, query_params), headers=self.build_header(), json=payload, data=data)
        return Result(r)

    def upload(self, end_point, **kwargs):
        r = self.session.post(self.get_url(end_point), **kwargs)
        return Result(r)


class Result(object):
    def __init__(self, response):
        self.response = response
        self.status_code = response.status_code
        self.reason = response.reason

    @property
    def data(self):
        self.response.raise_for_status()
        return self.response.json()

    def get(self, key, default_value=None):
        obj = self.data
        for key in key.split("."):
            if key not in obj:
                return default_value
            obj = obj[key]
        return obj

    def __str__(self):
        return self.__dict__.__str__()


ItemRange = namedtuple('ItemRange', 'start, end, total')


class PagedResult(Result):
    def __init__(self, response, callback, callback_kwargs, page_size=20):
        super(PagedResult, self).__init__(response)
        # range headers
        if 'item-range' in response.headers:
            if response.headers['item-range'] == "items */0":
                self.item_range = ItemRange(start=0, end=0, total=0)
            else:
                # in the format `items 0-9/100`
                items_part = response.headers['item-range'].split(' ')[1]
                range_part, total_part = items_part.split('/')
                start_part, end_part = range_part.split('-')
                self.item_range = ItemRange(start=int(start_part), end=int(end_part), total=int(total_part))

        self.callback = callback
        self.callback_kwargs = callback_kwargs
        self.page_size = page_size
        self._page = response.json().__iter__()
        self._data = None

    @property
    def data(self):
        self.response.raise_for_status()
        if not self._data:
            self._data = list(self)
        return self._data

    @property
    def datum(self):
        data = self.data
        if len(data) != 1:
            raise ValueError("Expected a single result but found %s result(s)." % len(data))
        return data[0]

    def __iter__(self):
        return self

    def increment_page(self):
        if not hasattr(self, 'item_range'):
            raise StopIteration

        next_start = self.item_range.end + 1
        next_end = min(self.item_range.end + self.page_size, self.item_range.total)

        if next_start >= self.item_range.total:
            raise StopIteration

        self.callback_kwargs.update(start=next_start, end=next_end)

        next_page = self.callback(**self.callback_kwargs)
        self.item_range = next_page.item_range
        self._page = next_page.response.json().__iter__()

    def __next__(self):
        return self.next()

    def next(self):
        try:
            return next(self._page)
        except StopIteration:
            self.increment_page()
            return next(self._page)
