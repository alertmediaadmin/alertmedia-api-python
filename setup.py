from setuptools import find_packages, setup

REQUIRES = ['requests >= 2.26.0']

setup(
    name='alertmedia',
    description='AlertMedia API client',
    author='AlertMedia',
    version='2.0.1',
    packages=find_packages(exclude=['tests', 'tests.*']),
    install_requires=REQUIRES,
    url='https://github.com/alertmedia/api_client_examples',
    license='',
    author_email='suppport@alertmedia.com',
    long_description="""\
    Python AlertMedia API Library
    ----------------------------

""",
)
