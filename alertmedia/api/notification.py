class Notification(object):
    API_LIST_NOTIFICATIONS = "/api/v5/notifications?customer={}"
    API_CREATE_NOTIFICATION = "/api/v5/notifications"
    API_UPDATE_NOTIFICATION = "/api/v5/notifications/{}"

    API_LIST_NOTIFICATION_PLANS = "/api/v5/notification_plans?ordering=title"
    API_GET_NOTIFICATION_PLAN = "/api/v5/notification_plans/{}"

    API_CREATE_NOTIFICATION_RESOURCE = "/api/notification_resources"

    def __init__(self, client):
        self.client = client
        self.net = client.net

    def get(self, notification_id):
        return self.net.get(self.API_UPDATE_NOTIFICATION.format(notification_id))

    def list(self, customer_id, start=None, end=None, user_id=None):
        """
        List of Notifications for the customer (requires Admin user)
        If user_id is passed in, lists Notifications only send to that User
        """
        url = self.API_LIST_NOTIFICATIONS.format(customer_id)
        if user_id:
            url += "?user={}".format(user_id)

        return self.net.get(url, start=start, end=end)

    def create(self, **kwargs):
        return self.net.post(self.API_CREATE_NOTIFICATION, payload=kwargs)

    def update(self, notification_id, **kwargs):
        return self.net.put(self.API_UPDATE_NOTIFICATION.format(notification_id), payload=kwargs)

    def get_plan(self, notification_plan_id):
        return self.net.get(self.API_GET_NOTIFICATION_PLAN.format(notification_plan_id))

    def list_plans(self, **kwargs):
        kwargs['customer'] = self.client.api_customer_id
        return self.net.list(self.API_LIST_NOTIFICATION_PLANS, query_params=kwargs)

    def create_attachment(self, name, file):
        return self.net.upload(self.API_CREATE_NOTIFICATION_RESOURCE, files={'notification_file': (name, file)})
